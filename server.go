package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"github.com/gomodule/redigo/redis"
	"github.com/satori/go.uuid"
)

// Store the redis connection as a package level variable
var cache redis.Conn
var db *mgo.Database

// COLLECTION is the name of the Mongo DB Collection
const (
	COLLECTION = "users"
)

// UsersDAO is a data access object type
type UsersDAO struct {
	Server   string
	Database string
}

//User is an event
type User struct {
	ID       bson.ObjectId `bson:"_id,omitempty"`
	Username string        `json:"username"`
	Password string        `json:"password"`
}

var user = &User{
	Username: "user1",
	Password: "password1",
}

//Connect connects you to mongodb
func (dao *UsersDAO) Connect() {
	session, err := mgo.Dial(dao.Server)
	if err != nil {
		log.Fatal(err)
	}
	db = session.DB(dao.Database)
}

// Insert a user into database
func (dao *UsersDAO) Insert(user User) error {
	err := db.C(COLLECTION).Insert(&user)
	return err
}

// Login logs a user in
func Login(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		http.ServeFile(w, r, "login.html")
	case "POST":
		var usr User
		// Get the JSON body and decode into credentials
		err := json.NewDecoder(r.Body).Decode(&usr)
		if err != nil {
			// If the structure of the body is wrong,
			// return an HTTP error
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		// Get the expected password from our in memory map
		// Make this get from Mongo
		result := User{}
		err = db.C(COLLECTION).Find(bson.M{"username": usr.Username}).One(&result)
		if err != nil {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		expectedPassword := result.Password

		// If a password exists for the given user
		// AND, if it is the same as the password we received,
		// then we can move ahead
		// if NOT, then we return an "Unauthorized" status
		if expectedPassword != usr.Password {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		// Create a new random session token
		byteSessionToken, _ := uuid.NewV4()
		sessionToken := byteSessionToken.String()
		// Set the token in the cache, along with the user
		// whom it represents
		// The token has an expiry time of 120 seconds
		_, err = cache.Do("SETEX", sessionToken, "120", usr.Username)
		if err != nil {
			// If there is an error in setting the cache,
			// return an internal server error
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		// Finally, we set the client cookie for "session_token"
		// as the session token we just generated
		// we also set an expiry time of 120 seconds,
		// the same as the cache
		http.SetCookie(w, &http.Cookie{
			Name:    "session_token",
			Value:   sessionToken,
			Expires: time.Now().Add(120 * time.Second),
		})
	}
}

// Welcome says hi
func Welcome(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte(fmt.Sprintln("Welcome Friend!")))
}

// Refresh does a refresh
func Refresh(w http.ResponseWriter, r *http.Request) {
	c, err := r.Cookie("session_token")
	if err != nil {
		if err == http.ErrNoCookie {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	sessionToken := c.Value

	response, err := cache.Do("GET", sessionToken)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if response == nil {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	// (END) The code uptil this point is the same as the first
	// part of the `Welcome` route

	// Now, create a new session token for the current user
	byteSessionToken, _ := uuid.NewV4()
	newSessionToken := byteSessionToken.String()
	_, err = cache.Do("SETEX", newSessionToken, "120", fmt.Sprintf("%s", response))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Delete the older session token
	_, err = cache.Do("DEL", sessionToken)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Set the new token as the users `session_token` cookie
	http.SetCookie(w, &http.Cookie{
		Name:    "session_token",
		Value:   newSessionToken,
		Expires: time.Now().Add(120 * time.Second),
	})
}

func main() {
	initCache()
	initDB()

	mux := http.NewServeMux()

	LoginHandler := http.HandlerFunc(Login)
	WelcomeHandler := http.HandlerFunc(Welcome)
	RefreshHandler := http.HandlerFunc(Refresh)

	mux.Handle("/refresh", RefreshHandler)
	mux.Handle("/login", LoginHandler)
	mux.Handle("/", ReqAuth(WelcomeHandler))

	log.Fatal(http.ListenAndServe(":8000", mux))
}

func initCache() {
	// Initialize the redis connection to a redis instance
	// running on your local machine
	conn, err := redis.DialURL("redis://localhost")
	if err != nil {
		panic(err)
	}
	// Assign the connection to the package level `cache` variable
	cache = conn
}

func initDB() {
	dao := &UsersDAO{
		Server:   "localhost",
		Database: "auth_test",
	}
	dao.Connect()
}

// ReqAuth requires authentication before running the handler
func ReqAuth(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// We can obtain the session token from the requests cookies, which come with every request
		c, err := r.Cookie("session_token")
		if err != nil {
			if err == http.ErrNoCookie {
				// If the cookie is not set, return an unauthorized status
				http.Redirect(w, r, "/login", http.StatusTemporaryRedirect)
				return
			}
			// For any other type of error, return a bad request status
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		sessionToken := c.Value

		// We then get the name of the user from our cache, where we set the session token
		response, err := cache.Do("GET", sessionToken)
		if err != nil {
			// If there is an error fetching from cache, return an internal server error status
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		if response == nil {
			// If the session token is not present in cache, return an unauthorized error
			http.Redirect(w, r, "/login", http.StatusTemporaryRedirect)
			return
		}
		h.ServeHTTP(w, r)
	})
}
